<?php
/**
 * Created by PhpStorm.
 * User: João Santos
 * Date: 09/04/15
 * Time: 10:39
 */

require_once libraries_get_path('google-api-php-client') . '/src/Google/Client.php';
require_once libraries_get_path('google-api-php-client') . '/src/Google/Service/Calendar.php';

class GoogleCalendarSync{

  protected static $user_email;
  protected static $service_id_client;
  protected static $service_account_email;
  protected static $service_account_pkcs12_file_path;

  protected static $calendar_ID;
  protected static $calendar_name;

  public $client;
  public $service;
  public $calendar_timezone;

  public $scope =  'https://www.googleapis.com/auth/calendar';

  function __construct() {

    // google_calendar_sync_calendar_id
    $this::$user_email                          = variable_get('google_calendar_sync_root_user_email', '');
    $this::$service_id_client                   = variable_get('google_calendar_sync_service_id_client', '');
    $this::$service_account_email               = variable_get('google_calendar_sync_service_account_email', '');
    $this::$service_account_pkcs12_file_path    = variable_get('google_calendar_sync_service_p12path', '');
    $this::$calendar_name                       = variable_get('google_calendar_sync_calendar_name', 'Events Calendar');

    $this->calendar_timezone                    = date_default_timezone();

    $this->buildGoogleClient();
    $service  = new Google_Service_Calendar($this->client);
    $this->service = $service;

    /* Create calendar if calendarID is not present */
    $current_Client_ID = variable_get('google_calendar_sync_calendar_id', '');

    if(!$current_Client_ID){

      $this->createCalendar($this::$calendar_name);

    }else{

      $this::$calendar_ID = $current_Client_ID;

    }

  }

  /**
   * build new client based on services authentication oauth2
   */
  private function buildGoogleClient(){

    $key = file_get_contents($this::$service_account_pkcs12_file_path);

    $auth = new Google_Auth_AssertionCredentials(
      $this::$service_account_email,
      array($this->scope),
      $key
    );

    $auth->sub = $this::$user_email;

    $client = new Google_Client();

    $client->setAssertionCredentials($auth);

    $this->client = $client;
  }


  /**
   * Create new calendar on Google calendars
   * @param $calendar_title
   * @return mixed
   */
  public function createCalendar($calendar_title){

    // Create Calendar.
    $calendar = new Google_Service_Calendar_Calendar();

    $calendar->setSummary($calendar_title);
    $calendar->setTimeZone($this->calendar_timezone);

    $createdCalendar = $this->service->calendars->insert($calendar);

    // Store calendar ID on current object.
    $this::$calendar_ID = $createdCalendar->getId();
    variable_set('google_calendar_sync_calendar_id', $this::$calendar_ID);

    // Create Calendar list entry
    $calendarListEntry = new Google_Service_Calendar_CalendarListEntry();

    $calendarListEntry->setId($this::$calendar_ID);


    $service = $this->service;
    $service->calendarList->insert($calendarListEntry);


  }


  /**
   * Create event on Google Calendar
   * @param $data
   * @return bool
   */
  public function createEvent($data){

    if($this->checkEvent($data['event_id'])) {

      $event = new Google_Service_Calendar_Event();
      $event->setSummary($data['summary']);
      $event->setLocation($data['location']);
      $event->setId($data['event_id']);
      $event->setVisibility('public');

      $start = new Google_Service_Calendar_EventDateTime();
      $start->setDateTime($data['start']);
      $start->setTimeZone($data['timezone']);
      $event->setStart($start);

      $end = new Google_Service_Calendar_EventDateTime();
      $end->setDateTime($data['end']);
      $end->setTimeZone($data['timezone']);
      $event->setEnd($end);

      $service = $this->service;
      $createdEvent = $service->events->insert($this::$calendar_ID, $event);

      return $createdEvent->getId();

    }else{

      // Update existent entry on Google Calendar.

      $service = $this->service;
      $event = $service->events->get($this::$calendar_ID, $data['event_id']);

      $event->setSummary($data['summary']);
      $event->setLocation($data['location']);
      $event->setVisibility('public');

      $start = new Google_Service_Calendar_EventDateTime();
      $start->setDateTime($data['start']);
      $start->setTimeZone($data['timezone']);
      $event->setStart($start);

      $end = new Google_Service_Calendar_EventDateTime();
      $end->setDateTime($data['end']);
      $end->setTimeZone($data['timezone']);
      $event->setEnd($end);

      $service->events->update($this::$calendar_ID, $event->getId(), $event);

      return $event->getId();

    }

  }

  /**
   * Check if this events is already on Google Calendar
   * @param $event_id
   *
   * @return bool true if calendar don't exists
   */
  private function checkEvent($event_id){

    try {

      $service = $this->service;
      $service->events->get($this::$calendar_ID, $event_id);

    } catch (Exception $e) {

      return TRUE;

    }

    return FALSE;

  }

  public function delete($event_id){
    $service = $this->service;
    $service->events->delete($this::$calendar_ID, $event_id);
  }


  public function listCalendars(){
    $service = $this->service;
    $info = array();

    $calendarList = $service->calendarList->listCalendarList();

    while(true) {

      foreach ($calendarList->getItems() as $calendarListEntry) {
        $info['calendars'][] = $calendarListEntry->getSummary();
      }

      $pageToken = $calendarList->getNextPageToken();

      if ($pageToken) {

        $optParams = array('pageToken' => $pageToken);
        $calendarList = $service->calendarList->listCalendarList($optParams);

      }
      else {
        break;
      }
    }

    $settings = $service->settings->listSettings();

    foreach ($settings->getItems() as $setting) {
      $info['settings'][] = $setting->getId() . ': ' . $setting->getValue();
    }

    return $info;

  }

}
