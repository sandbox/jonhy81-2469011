<?php
/**
 * @file
 * Administration pages for Google Calendar Sync settings.
 *
 *
 * @param array $form_state
 *   An associative array containing the current state of the form.
 * @param int $id
 *   An id of the account.
 *
 * @ingroup forms
 * @see google_calendar_sync_edit_form_submit()
 * @see google_calendar_sync_edit_form_validate()
 *
 * @todo add calendarID to form
 * @todo Add dynamic field picker and content type
 */

/**
 * Form builder; Edit an account.
 *
 * @param array $form
 * An associative array containing the current the form.
 * @param array $form_state
 * An associative array containing the current state of the form.
 * @param int $id
 *   An id of the account.
 *
 * @ingroup forms
 * @see google_calendar_sync_settings_form_validate()
 * @see google_calendar_sync_settings_form_submit()
 */
function google_calendar_sync_settings_form($form, &$form_state, $id = NULL) {
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  $form['google_calendar_sync_calendar_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Google Calendar Sync.'),
    '#default_value' => variable_get('google_calendar_sync_calendar_enabled', FALSE),
  );

  $form['google_calendar_sync_calendar_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Calendar title'),
    '#description' => t('The name for the calendar.'),
    '#default_value' => variable_get('google_calendar_sync_calendar_name', ''),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['google_calendar_sync_calendar_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Calendar id'),
    '#description' => t('Google calendar id.'),
    '#default_value' => variable_get('google_calendar_sync_calendar_id', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['google_calendar_sync_root_user_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Google email'),
    '#description' => t('The root user email of google account.'),
    '#default_value' => variable_get('google_calendar_sync_root_user_email', ''),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['google_calendar_sync_service_id_client'] = array(
    '#type' => 'textfield',
    '#title' => t('Service Id Client'),
    '#description' => t('The Service Id client of google service credentials.'),
    '#default_value' => variable_get('google_calendar_sync_service_id_client', ''),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['google_calendar_sync_service_account_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Service account email'),
    '#description' => t('The service account email of google service credentials.'),
    '#default_value' => variable_get('google_calendar_sync_service_account_email', ''),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['google_calendar_sync_service_p12path'] = array(
    '#type' => 'textfield',
    '#title' => t('P12 Key path'),
    '#description' => t('Path to .p12 file relative to drupal installations.'),
    '#default_value' => variable_get('google_calendar_sync_service_p12path', ''),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="google_calendar_sync_calendar_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);

}

/**
 * Validate handler for adding a new account to google auth accounts.
 */
function google_calendar_sync_settings_form_validate($form, &$form_state) {

  // @todo Validate credentials on settings save

}

/**
 * Submit handler for adding a new account to google auth accounts.
 */
function google_calendar_sync_settings_form_submit($form, &$form_state) {

  drupal_set_message(t('Google Calendar settings saved'));

  $form_state['redirect'] = 'admin/config/services/google_calendar_sync';

}
